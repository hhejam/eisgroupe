package com.eisgroupe.word.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.service.SearchFacadeService;
import com.eisgroupe.word.service.SentenceService;
import com.eisgroupe.word.service.WordService;

@Service
public class SearchServiceFacadeImpl implements SearchFacadeService {
	
	private final Logger log = LoggerFactory.getLogger(SearchServiceFacadeImpl.class);
	
	@Autowired
	private SentenceService sentenceService;
	
	@Autowired
	private WordService wordService;

	@Override
	public List<Word> searchWordByClient(String clientID) {
		log.debug("Search word by client id {}", clientID);
		return this.wordService.findByClient(clientID);
	}

	@Override
	public List<Word> searchWordByValueLike(String word) {
		log.debug("Search word that contains {}", word);
		return this.wordService.findLike(word);
	}

	@Override
	public List<Sentence> searchSentenceByClient(String clientID) {
		log.debug("Search sentence for the client {}", clientID);
		return this.sentenceService.findByClient(clientID);
	}

	@Override
	public List<Sentence> searchSentenceByWordLike(String word) {
		log.debug("Search sentence that contains word like {}", word);
		return this.sentenceService.findByWordLike(word);
	}


}
