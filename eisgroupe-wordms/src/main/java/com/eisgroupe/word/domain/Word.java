package com.eisgroupe.word.domain;

import java.io.Serializable;
import java.time.Instant;

import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.CassandraType.Name;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.SASI;
import org.springframework.data.cassandra.core.mapping.SASI.IndexMode;
import org.springframework.data.cassandra.core.mapping.Table;

import com.eisgroupe.common.domain.exchange.WordStatus;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * A Word.
 */
@Getter 
@Setter 
@ToString 
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@XStreamAlias("word")
@Builder
@Table
public class Word implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column 
    @PrimaryKey
    private String id;

    @Indexed
    @SASI(indexMode = IndexMode.CONTAINS)
    @SASI.StandardAnalyzed
    private String value;
    
    @Column
    private Instant creationTime;
    
    @Column
    private Instant concatenationTime;
    
    @CassandraType(type = Name.VARCHAR) 
    private WordStatus status;

    @XStreamAlias("client")
    @Transient
    private Client client;
    
   
}
