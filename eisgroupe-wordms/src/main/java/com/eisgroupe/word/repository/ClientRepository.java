package com.eisgroupe.word.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.Client;


/**
 * Spring Data Cassandra repository for the client entity.
 */
@Repository
public interface ClientRepository extends CassandraRepository<Client, String> {
}
