package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;
import java.time.Instant;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 * A domain class to To represent the query Cassandra for sentence by client.
 */
@Table
public class SentenceByClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    private SentenceByClientKey key;
    
    @Column
    private String value;

    @Column
    private Instant creationTime;

	public SentenceByClientKey getKey() {
		return key;
	}

	public void setKey(SentenceByClientKey key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}
   
}
