package com.eisgroupe.processor.kafka.aggregration;

import org.apache.kafka.streams.kstream.Aggregator;

import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
@Deprecated
public class WordAggregator implements Aggregator<String, WordExchange, SentenceExchange> {

	@Override
	public SentenceExchange apply(String key, WordExchange value, SentenceExchange aggregate) {
		aggregate.getWords().add(value);
		return aggregate;
	}

}
