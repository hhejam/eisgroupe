  

# Components:

The project is composed in 3 parts :

*  **eisgroupe-common** : Contains the common utilities and the shared composants/

*  **eisgroupe-processor** : The processor that receive word and concatenate the word on sentence.

*  **eisgroupe-wordms** : Tha main microservice acting as the entry point. This service expose a Rest Api for sending word and searching words and sentences. This folder contains also the docker files needed to start the runtime environment.

# Kafka Topics:

The project need 2 topics :

* ***topic-in*** : used as the input topic for sending words

* ***topic-out***: used to exchange the sentences by the processor and the wordms.

Topics can be created manually or automatically by a docker-compose file present on the **wordms/src/main/resources/docker**

# Start The runtime environment:

Start the different components of the runtime environment :

* Zookeeper

* Kafka broker

* Cassandra

This step can de done by running docker compose on the folder **eisgroupe-wordms/src/main/resources/docker**:

	  cd eisgroupe-wordms/src/main/resources/docker
	  docker-compose -f main.yml up

  

***/!\ Limitation***: Please note that sometimes it will be necessary to run once again the  docker service ***brocker-setup (main.yml)*** separately be able to create the topics needed by the projects .

  

# Configuration:

The 3 projects are based on the spring boot configuration and so most of the configuration of these projects are in the files ***application.yml***.

The configuration of the integration layer is located on the Camel configuration files ***src/main/resources/camel-routes/kafka-route.xml***

## Processor configuration
* **bootstrap-server** : The broker server address and port  is configured in the yaml file application.yml by the propertie topic-in.
* **topic-in** : The name of the input topic is configured in the yaml file application.yml by the propertie topic-in.
 * **topic-out** : The name of the output topic is configured in the yaml file application.yml by the propertie topic-out.
 * **window-interval** : The window interval is configured in the yaml file application.yml by the propertie window-interval, by default is 6000 ms.

## Word microservice configuration
* **bootstrap-server** : The broker server address and port  is configured in the yaml file application.yml by the propertie topic-in
* **topic-in** : The name of the input topic is configured in the yaml file application.yml by the propertie topic-in
 * **topic-out** : The name of the output topic is configured in the yaml file application.yml by the propertie topic-out
 * **Http server and port** : The port of the Http server  is configured in the yaml file application.yml by the propertie server.port, by default **8881**
 * **Database server contact-points** : The database server  contact-points is configured in the yaml file application.yml by the propertie data.cassandra.contact-points, by default **127.0.0.1**
 *  **Database server port** : The database server  port is configured in the yaml file application.yml by the propertie data.cassandra.ports, by default **9042**
 *  **Database keyspace-name** : The database server  keyspace-name is configured in the yaml file application.yml by the propertie data.cassandra.keyspace-name, by default **eistest**
 * **Database schema auto creation**: The database can be auto created by Spring data by setting the property data.cassandra.schema-action to **CREATED**, once the schema is created the value must be **NONE**
  

# Build and Run:

The 3 project must be built and install by the command

	 mvn clean insatll

The order of build is as following:

*  ***eisgroupe-common***

*  **eisgroupe-processor** or **eisgroupe-wordms**

  

Then the 2 projects **eisgroupe-processor** and **eisgroupe-wordms** must be run by the command:

	mvn spring-boot:run

 # Rest API Documentation
The project also provide a swagger documentation on the url 	
[http://localhost:8881/swagger-ui.html](http://localhost:8881/swagger-ui.html) or it can be viewed directly on gitlab by clicking the file **[swagger.yaml](https://gitlab.com/hhejam/eisgroupe/-/blob/master/swagger.yaml)** 

# Test:

The project contains a set of postman request used to test the different use cases, we have just to import the file present on the root named ***eisgroup-postman.json***


# Smoke Tests:

The project can be also tested by running the smoke test present on the ***eisgroupe-wordms*** by Junit or mvn test

The smoke test clean the database before the execution in order to have a clean state before the test.

## Smoke test cases suite:
***step1*** : Send  the words hello and  world, the expected result is 2 words stocked on the database and 1 sentence "Hello world"

***step2*** :Send the words bonjour,  tout,  le,  and monde, the expected result is 6 words stocked on the database and 2 sentences "Hello world" and "Bonjour tout le monde"

***step3*** :Search sentences by a pattern (Like search).

***step4*** :Search sentences by a client id.

***step5*** :Search words with a pattern (Like search).

***step6*** :Search words by a client id.


 **/!\ limitation** : The search is case sensitive.

### Reference Documentation

  

For further reference, please consider the following sections:

  

*  [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)

  

*  [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/)

  

*  [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/#build-image)

  

* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/reference/htmlsingle/#production-ready

### Guides

  

The following guides illustrate how to use some features concretely:

  

*  [Using Apache Camel with Spring Boot](https://camel.apache.org/camel-spring-boot/latest/spring-boot.html)

  

*  [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)