package com.eisgroupe.word.domain.mapper.cassandra.query;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.cassandra.query.SentenceByClient;

@Mapper(componentModel = "spring")
public interface SentenceByClientMapper {

	@Mapping(source = "sentence.id", target = "key.sentenceID")
	@Mapping(source = "sentence.client.id", target = "key.clientID")
	SentenceByClient to(Sentence sentence);

}
