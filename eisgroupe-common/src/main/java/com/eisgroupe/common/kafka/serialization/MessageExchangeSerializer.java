package com.eisgroupe.common.kafka.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eisgroupe.common.domain.exchange.ClientExchange;
import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;

public class MessageExchangeSerializer<T> implements Serializer<MessageExchange<T>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageExchangeSerializer.class);
	
	private XStream xstream = new XStream(new StaxDriver());

	@Override
	public void configure(Map<String, ?> map, boolean b) {
		xstream.autodetectAnnotations(true);
		XStream.setupDefaultSecurity(xstream);
		xstream.addPermission(AnyTypePermission.ANY);
		xstream.ignoreUnknownElements();
		xstream.processAnnotations(MessageExchange.class);
		xstream.processAnnotations(ClientExchange.class);
		xstream.processAnnotations(SentenceExchange.class);
		xstream.processAnnotations(WordExchange.class);
	}

	@Override
	public byte[] serialize(String arg0, MessageExchange<T> arg1) {
		LOGGER.info("Serializing message wrapper {}", arg1);
		byte[] retVal = null;
		try {
			retVal = xstream.toXML(arg1).getBytes();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return retVal;
	}

	@Override
	public void close() {
	}
}