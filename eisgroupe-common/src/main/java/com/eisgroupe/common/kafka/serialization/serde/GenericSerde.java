package com.eisgroupe.common.kafka.serialization.serde;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

@Deprecated
public class GenericSerde<T> implements Serde<T> {

	private Class<T> type;
	
	public GenericSerde() {
		
	}
	
	public GenericSerde(Class<T> type) {
		this.type = type;
	}

	@Override
	public Serializer<T> serializer() {
		return new POJOSerializer<T>();
	}

	@Override
	public Deserializer<T> deserializer() {
		return new POJODeserializer<T>(this.type);
	}

}