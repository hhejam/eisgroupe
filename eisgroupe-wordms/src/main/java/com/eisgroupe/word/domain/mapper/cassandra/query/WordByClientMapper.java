package com.eisgroupe.word.domain.mapper.cassandra.query;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.WordByClient;

@Mapper(componentModel = "spring")
public interface WordByClientMapper {

	@Mapping(source = "word.client.id", target = "key.clientID")
	@Mapping(source = "word.id", target = "key.wordID")
    WordByClient to(Word word);

}
