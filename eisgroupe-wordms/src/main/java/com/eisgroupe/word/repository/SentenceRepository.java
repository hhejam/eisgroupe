package com.eisgroupe.word.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.repository.cassandra.RepositoryBluckOperation;

/**
 * Spring Data Cassandra repository for the Sentence entity.
 */
@Repository
public interface SentenceRepository extends CassandraRepository<Sentence, String>, RepositoryBluckOperation<Sentence> {

	List<Sentence> findByValueLike(String name);
}
