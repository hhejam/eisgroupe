package com.eisgroupe.word.repository.cassandra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;

public class CustomizedSaveImpl<T> implements RepositoryBluckOperation<T> {

	@Autowired
	private CassandraOperations operations;

	@Override
	public <S extends T> S save(S entity) {
		InsertOptions insertOptions = org.springframework.data.cassandra.core.InsertOptions.builder().build();
		operations.insert(entity, insertOptions);
		return entity;
	}
}