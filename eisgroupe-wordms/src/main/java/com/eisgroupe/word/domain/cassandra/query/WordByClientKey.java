package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

/**
 * A domain class to To represent the query Cassandra for word by client.
 */
@PrimaryKeyClass
public class WordByClientKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String clientID;
    
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED,  ordinal = 1, ordering = Ordering.DESCENDING)
    private String wordID;

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getWordID() {
		return wordID;
	}

	public void setWordID(String wordID) {
		this.wordID = wordID;
	}
    
   
}
