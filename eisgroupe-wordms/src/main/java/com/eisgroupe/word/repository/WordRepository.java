package com.eisgroupe.word.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.repository.cassandra.RepositoryBluckOperation;


/**
 * Spring Data Cassandra repository for the Word entity.
 */
@Repository
public interface WordRepository extends CassandraRepository<Word, String>, RepositoryBluckOperation<Word> {
	List<Word> findByValueLike(String name);
}
