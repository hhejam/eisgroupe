package com.eisgroupe.processor.kafka.exchange;

import java.time.Instant;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.formatter.date.DateFormatter;

@Deprecated
public class WordDatetimeExtractor implements TimestampExtractor {

	@Override
	public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {
		String timestampAsString = ((MessageExchange<?>)record.value()).getTimestamp();
		Instant result = DateFormatter.parse(timestampAsString);
		return result.toEpochMilli();
	}

}