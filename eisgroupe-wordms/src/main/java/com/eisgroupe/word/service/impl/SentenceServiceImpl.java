package com.eisgroupe.word.service.impl;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordStatus;
import com.eisgroupe.word.domain.Client;
import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.SentenceByClient;
import com.eisgroupe.word.domain.cassandra.query.SentenceByWord;
import com.eisgroupe.word.domain.mapper.SentenceMapper;
import com.eisgroupe.word.repository.ClientRepository;
import com.eisgroupe.word.repository.SentenceByClientRepository;
import com.eisgroupe.word.repository.SentenceByWordRepository;
import com.eisgroupe.word.repository.SentenceRepository;
import com.eisgroupe.word.repository.WordRepository;
import com.eisgroupe.word.service.SentenceService;

/**
 * Service Implementation for managing {@link Sentence}.
 */
@Service(value = "sentenceService")
public class SentenceServiceImpl implements SentenceService {

	private final Logger log = LoggerFactory.getLogger(SentenceServiceImpl.class);

	@Autowired
	private  SentenceRepository sentenceRepository;
	
	@Autowired
	private  SentenceByClientRepository sentenceByClientRepository;
	
	@Autowired
	private  SentenceByWordRepository sentenceByWordRepository;

	@Autowired
	private  WordRepository wordRepository;
	
	@Autowired
	private  ClientRepository clientRepository;
	
	@Autowired
	private  SentenceMapper sentenceMapper;


	@SuppressWarnings("unchecked")
	public void listen(@Body MessageExchange<SentenceExchange> messageExchange) {
		log.debug("Received Messasge : " + messageExchange + " on the " + new Date());
		List<SentenceExchange> sentenceExchanges = (List<SentenceExchange>) messageExchange.getPayload();
		sentenceExchanges.stream()
						 .map(sentenceMapper::from)
						 .forEach(sentence -> this.save(sentence));
	}

	@Override
	public Sentence save(Sentence sentence) {
		if (sentence.getId() == null) {
			sentence.setId(UUID.randomUUID().toString());
		}
		if (sentence.getWords() != null && !sentence.getWords().isEmpty()) {
			sentence.getWords().stream().forEach(word -> {
				word.setStatus(WordStatus.READY);
				word.setConcatenationTime(Instant.now());
				this.wordRepository.save(word);
			});
		}
		return this.sentenceRepository.save(sentence);
	}

	@Override
	public List<Sentence> findAll() {
		return this.sentenceByClientRepository
				   .findAll()
				   .stream()
				   .map(sentenceByClient -> this.enrichSentenceByClient(sentenceByClient))
				   .collect(Collectors.toList());
	}
	
	@Override
	public List<Sentence> findByWordLike(String word) {
		List<SentenceByWord>  sentenceByWords = this.sentenceByWordRepository.findByWordLike("%" + word + "%");
		return sentenceByWords.stream()
						.map(sentenceByWord -> this.enrichSentenceByWord(sentenceByWord))
						.collect(Collectors.toList());
		
	}
	
	@Override
	public List<Sentence> findByValueLike(String search) {
		return this.sentenceRepository.findByValueLike("%" + search + "%");
	}
	
	@Override
	public List<Sentence> findByClient(String clientID) {
		return this.sentenceByClientRepository
				   .findByKeyClientID(clientID)
				   .stream()
				   .map(sentenceByClient -> this.sentenceRepository.findById(sentenceByClient.getKey().getSentenceID()))
				   .filter(Optional::isPresent)
	               .map(Optional::get)
	               .map(sentence -> this.enrichClient(clientID, sentence))
				   .collect(Collectors.toList());
		
	}
	
	private Sentence enrichSentenceByClient(SentenceByClient sentenceByClient) {
		Optional<Sentence>  sentenceOptional = 
				this.sentenceRepository.findById(sentenceByClient.getKey().getSentenceID());
		if( sentenceOptional.isPresent()) {
			Sentence sentence = sentenceOptional.get();
			this.enrichClient(sentenceByClient.getKey().getClientID(), sentence);
			this.enrichWord(sentence);
			return sentence;
		}
		return  null;
	}
	
	private Sentence enrichSentenceByWord(SentenceByWord sentenceByWord) {
		Optional<Sentence> sentenceOptional = this.sentenceRepository.findById(sentenceByWord.getKey().getSentenceID());
		if(sentenceOptional.isPresent()) {
			Sentence sentence = sentenceOptional.get();
			this.enrichWord(sentence);
			this.enrichClient(sentenceByWord.getClientID(), sentence);
			return sentence;
		}
		return null;
	}
	
	private Sentence enrichWord(Sentence sentence) {
		List<Word> words =	this.sentenceByWordRepository
								.findByKeySentenceID(sentence.getId())
								.stream()
								.map(sentenceByWord -> 
										this.wordRepository.findById(sentenceByWord.getKey().getWordID()))
								.filter(Optional::isPresent)
				                .map(Optional::get)
								.collect(Collectors.toList());
		sentence.setWords(words);
		return sentence;
	}

	
	private Sentence enrichClient(String clientID, Sentence sentence) {
		Optional<Client> clientOptional = this.clientRepository.findById(clientID);
		if( clientOptional.isPresent()) {
			sentence.setClient(clientOptional.get());
		}
		return sentence;
	}
	
	@Override
	public Optional<Sentence> findOne(String id) {
		return this.sentenceRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		this.sentenceRepository.deleteById(id);
	}

}
