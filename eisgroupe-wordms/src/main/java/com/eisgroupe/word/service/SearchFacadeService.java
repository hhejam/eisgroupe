package com.eisgroupe.word.service;

import java.util.List;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;

/**
 * Service Interface for searching {@link Sentence} and {@link Word}.
 */
public interface SearchFacadeService {

	 /**
     * Get all the Words for a client.
     *
     * @return the list of {@link Word}.
     */
    List<Word> searchWordByClient(String clientID);
    
    /**
     * Get all the Words for a client.
     *
     * @return the list of {@link Word}.
     */
    List<Word> searchWordByValueLike(String clientID);
    
    /**
     * Get all the sentence for a client.
     *
     * @return the list of {@link Sentence}.
     */
    List<Sentence> searchSentenceByClient(String clientID);
        
    /**
     * search a list of sentences that containes a word.
     *
     * @param word query.
     * @return the {@link List} of {@link Sentence}.
     */
    List<Sentence> searchSentenceByWordLike(String word);

}
