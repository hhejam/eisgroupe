package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

/**
 * A domain class to To represent the query Cassandra for word by client.
 */
@PrimaryKeyClass
public class SentenceByWordKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String sentenceID;
    
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED,  ordinal = 1, ordering = Ordering.DESCENDING)
    private String wordID;

	public String getSentenceID() {
		return sentenceID;
	}

	public void setSentenceID(String sentenceID) {
		this.sentenceID = sentenceID;
	}

	public String getWordID() {
		return wordID;
	}

	public void setWordID(String wordID) {
		this.wordID = wordID;
	}
    
}
