package com.eisgroupe.word.repository.cassandra;

public interface RepositoryBluckOperation<T> {
	<S extends T> S save(S entity);
}
