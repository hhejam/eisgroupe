package com.eisgroupe.word.service;

import java.util.List;
import java.util.Optional;

import com.eisgroupe.word.domain.Client;
import com.eisgroupe.word.domain.Sentence;

/**
 * Service Interface for managing {@link Sentence}.
 */
public interface SentenceService {

    /**
     * Save a {@link Sentence}.
     *
     * @param  {@link Sentence} the entity to save.
     * @return the persisted {@link Sentence}.
     */
    Sentence save(Sentence sentence);

    /**
     * Get all the {@link Sentence}.
     *
     * @return the {@link List} of {@link Sentence}.
     */
    List<Sentence> findAll();
    
    /**
     * Get the "id" {@link Sentence}.
     *
     * @param id the id of the sentence.
     * @return the {@link Sentence}.
     */
    Optional<Sentence> findOne(String id);
    
    /**
     * Get a list of {@link Sentence} that containes a word.
     *
     * @param word query.
     * @return the entity.
     */
    List<Sentence> findByWordLike(String word);
    
    /**
     * Get the list of {@link Sentence} by {@link Client} id.
     *
     * @param id of the {@link Client}.
     */
	List<Sentence> findByClient(String clientID);
	
	/**
     * searcg the list of {@link Sentence} that contains chars.
     *
     * @param search query.
     */
	List<Sentence> findByValueLike(String search);

    /**
     * Delete the "id" sentence.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
    
}
