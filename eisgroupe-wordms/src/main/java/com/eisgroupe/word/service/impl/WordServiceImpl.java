package com.eisgroupe.word.service.impl;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eisgroupe.common.domain.exchange.WordExchange;
import com.eisgroupe.common.domain.exchange.WordStatus;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.WordByClient;
import com.eisgroupe.word.domain.mapper.WordMapper;
import com.eisgroupe.word.repository.ClientRepository;
import com.eisgroupe.word.repository.WordByClientRepository;
import com.eisgroupe.word.repository.WordExchangeRepository;
import com.eisgroupe.word.repository.WordRepository;
import com.eisgroupe.word.service.WordService;

/**
 * Service Implementation for managing {@link Word}.
 */
@Service
public class WordServiceImpl implements WordService {

	private final Logger log = LoggerFactory.getLogger(WordServiceImpl.class);

	@Autowired
	private WordExchangeRepository<WordExchange> wordExchangeRepository;

	@Autowired
	private WordRepository wordRepository;

	@Autowired
	private WordByClientRepository wordByClientRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private WordMapper wordMapper;

	/**
	 * Save a word.
	 *
	 * @param word the entity to save.
	 * @return the persisted entity.
	 */
	@Override
	public Word save(Word word) {
		log.debug("Request to save Word : {}", word);

		if (word.getId() == null) {
			word.setId(UUID.randomUUID().toString());
			word.setStatus(WordStatus.NEW);
			word.setCreationTime(Instant.now());
		}

		WordExchange wordExchange = this.wordMapper.to(word);
		this.wordExchangeRepository.save(wordExchange);
		return this.wordRepository.save(word);
	}

	@Override
	public Optional<Word> findOne(String id) {
		return this.wordRepository.findById(id);
	}
	
	@Override
	public List<Word> findAll() {
		return this.wordByClientRepository.findAll()
				                          .stream()
				                          .map(wordByClient -> enrichWord(wordByClient))
				                          .filter(Optional::isPresent)
				                          .map(Optional::get)
				          				  .collect(Collectors.toList());
				
								  
	}

	@Override
	public List<Word> findByClient(String clientID) {
		log.debug("Request to find words by client id : {}", clientID);
		if (StringUtils.isEmpty(clientID)) {
			throw new IllegalStateException("The client id cannot be null or empty");
		}
		List<WordByClient> wordByClients = this.wordByClientRepository.findByKeyClientID(clientID);
		return wordByClients.stream()
							.filter(wordByClient -> wordByClient.getKey() != null)
							.map(wordByClient -> enrichWord(wordByClient))
							.filter(Optional::isPresent)
							.map(Optional::get)
							.collect(Collectors.toList());
	}

	private Optional<Word> enrichWord(WordByClient wordByClient) {
		Optional<Word> wordOptional = this.wordRepository.findById(wordByClient.getKey().getWordID());
		if (wordOptional.isPresent()) {
			Word word = wordOptional.get();
			word.setClient(this.clientRepository.findById(wordByClient.getKey().getClientID()).get());
			return Optional.<Word>of(word);
		}
		return Optional.<Word>empty();
	}
	
	@Override
	public List<Word> findLike(String query) {
		return this.wordRepository.findByValueLike("%" + query + "%");
	}

	@Override
	public void delete(String id) {
		this.wordRepository.deleteById(id);
	}
	

}
