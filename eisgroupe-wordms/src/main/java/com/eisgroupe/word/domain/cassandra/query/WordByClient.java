package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;
import java.time.Instant;

import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.CassandraType.Name;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.eisgroupe.common.domain.exchange.WordStatus;

/**
 * A domain class to To represent the query Cassandra for word by client.
 */
@Table
public class WordByClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    private WordByClientKey key;
    
    @Column
    private String value;
    
    @Column
    private Instant creationTime;
    
    @Column
    private Instant concatenationTime;
    
    @CassandraType(type = Name.VARCHAR) 
    private WordStatus status;

	public WordByClientKey getKey() {
		return key;
	}

	public void setKey(WordByClientKey key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	public Instant getConcatenationTime() {
		return concatenationTime;
	}

	public void setConcatenationTime(Instant concatenationTime) {
		this.concatenationTime = concatenationTime;
	}

	public WordStatus getStatus() {
		return status;
	}

	public void setStatus(WordStatus status) {
		this.status = status;
	}
    
   
}
