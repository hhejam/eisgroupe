package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

/**
 * A domain class to To represent the query Cassandra for sentence by client.
 */
@PrimaryKeyClass
public class SentenceByClientKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String sentenceID;
    
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED,  ordinal = 1, ordering = Ordering.DESCENDING)
    private String clientID;

	public String getSentenceID() {
		return sentenceID;
	}

	public void setSentenceID(String sentenceID) {
		this.sentenceID = sentenceID;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
   
    
}
