package com.eisgroupe.word.service;

import java.util.List;
import java.util.Optional;

import com.eisgroupe.word.domain.Word;

/**
 * Service Interface for managing {@link Word}.
 */
public interface WordService {

	 /**
     * Save a Word.
     *
     * @param Word the entity to save.
     */
    Word save(Word Word);

    /**
     * Get all the Words.
     *
     * @return the list of Word.
     */
    List<Word> findAll();
    
    /**
     * Get all the Words like a word.
     *
     * @return the list of Word.
     */
    List<Word> findLike(String query);
    
    /**
     * Get all the Words like a word.
     *
     * @return the list of Word.
     */
    List<Word> findByClient(String clientID);
    
    /**
     * Get the "id" Word.
     *
     * @param id the id of the Word.
     * @return the entity.
     */
    Optional<Word> findOne(String id);

    /**
     * Delete the "id" Word.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

}
