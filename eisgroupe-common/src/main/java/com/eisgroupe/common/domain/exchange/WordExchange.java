package com.eisgroupe.common.domain.exchange;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * A value object for sending and receiving Word.
 */
@Getter 
@Setter 
@ToString 
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XStreamAlias("word")
public class WordExchange implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    
    private String exchangeId;

    private String value;
    
    private String creationTime;
    
    private String concatenationTime;
    
    private String status;

    private String clientID;
   
}
