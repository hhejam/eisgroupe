package com.eisgroupe.word.domain.mapper.cassandra.query;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.SentenceByWord;

@Mapper(componentModel = "spring")
public interface SentenceByWordMapper {

	@Mapping(source = "sentence.value", target = "sentence")
	@Mapping(source = "word.value", target = "word")
	@Mapping(source = "sentence.creationTime", target = "creationTime")
	@Mapping(source = "sentence.client.id", target = "clientID")
	@Mapping(source = "sentence.id", target = "key.sentenceID")
	@Mapping(source = "word.id", target = "key.wordID")
	SentenceByWord to(Sentence sentence, Word word);

}
