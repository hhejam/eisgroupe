package com.eisgroupe.processor;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
import com.eisgroupe.processor.camel.processor.ExchangeMessageTransfomerImpl;
import com.eisgroupe.processor.camel.processor.Transfomer;

class ProcessorApplicationTests {
	
	private Transfomer<List<WordExchange>, MessageExchange<List<SentenceExchange>>> transformer =
			new ExchangeMessageTransfomerImpl();
	
	
	
	@Test
	void transform() {
		WordExchange client1Word1 =	WordExchange.builder()
												 .clientID("001")
												 .status("NEW")
												 .value("client1Word1")
												 .build();
		
		WordExchange client1Word2 =	WordExchange.builder()
												 .clientID("001")
												 .status("NEW")
												 .value("client1Word2")
												 .build();
		
		WordExchange client2Word1 =	WordExchange.builder()
												 .clientID("002")
												 .status("NEW")
												 .value("client2Word1")
												 .build();
		
		WordExchange client2Word2 =	WordExchange.builder()
												 .clientID("002")
												 .status("NEW")
												 .value("client2Word2")
												 .build();
		MessageExchange<List<SentenceExchange>> messageExchange = transformer.transform(
														Arrays.asList(client1Word1, client1Word2, client2Word1, client2Word2));
		assertNotNull(messageExchange);
		assertNotNull(messageExchange.getPayload());
		assertEquals(2, messageExchange.getPayload().size());
		
		List<SentenceExchange> sentenceExchanges1 = messageExchange.getPayload();
		Optional<SentenceExchange> sentenceExchangeOptional1 = sentenceExchanges1.stream()
				.filter(s -> s.getClientID() == "001").findFirst();
		assertTrue(sentenceExchangeOptional1.isPresent());
		assertNotNull(sentenceExchangeOptional1.get().getWords());
		assertEquals(2, sentenceExchangeOptional1.get().getWords().size());
		WordExchange client1Word1Expected = sentenceExchangeOptional1.get().getWords()
									   .stream().filter(w -> w.getValue() == client1Word1.getValue()).findFirst().get();
		assertNotNull(client1Word1Expected);
		WordExchange client1Word2Expected = sentenceExchangeOptional1.get().getWords()
				   .stream().filter(w -> w.getValue() == client1Word2.getValue()).findFirst().get();
		assertNotNull(client1Word2Expected);
		

		List<SentenceExchange> sentenceExchanges2 = messageExchange.getPayload();
		Optional<SentenceExchange> sentenceExchangeOptional2 = sentenceExchanges2.stream()
				.filter(s -> s.getClientID() == "002").findFirst();
		assertTrue(sentenceExchangeOptional2.isPresent());
		assertNotNull(sentenceExchangeOptional2.get().getWords());
		assertEquals(2, sentenceExchangeOptional2.get().getWords().size());
		WordExchange client2Word1Expected = sentenceExchangeOptional2.get().getWords()
									   .stream().filter(w -> w.getValue() == client2Word1.getValue()).findFirst().get();
		assertNotNull(client2Word1Expected);
		WordExchange client2Word2Expected = sentenceExchangeOptional2.get().getWords()
				   .stream().filter(w -> w.getValue() == client2Word2.getValue()).findFirst().get();
		assertNotNull(client2Word2Expected);
		
	
	}

}
