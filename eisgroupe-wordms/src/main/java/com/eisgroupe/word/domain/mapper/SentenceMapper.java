package com.eisgroupe.word.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.word.domain.Sentence;


@Mapper(componentModel = "spring", uses = {WordMapper.class, ClientMapper.class})
public interface SentenceMapper {

	@Mapping(source = "sentence.client.id", target = "clientID")
    SentenceExchange to(Sentence sentence);
    
	@Mapping(source = "sentenceExchange.clientID", target = "client.id")
    Sentence from(SentenceExchange sentenceExchange);
}
