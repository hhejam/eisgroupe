package com.eisgroupe.word.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.cassandra.query.WordByClient;
import com.eisgroupe.word.domain.cassandra.query.WordByClientKey;


/**
 * Spring Data Cassandra repository for query the Word by client Id.
 */
@Repository
public interface WordByClientRepository extends CassandraRepository<WordByClient, WordByClientKey> {
	
	@AllowFiltering
	List<WordByClient> findByKeyClientID(String clientID);
	
}
