package com.eisgroupe.common.domain.exchange;

import java.io.Serializable;
import java.util.UUID;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString 
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XStreamAlias("message")
public class MessageExchange<T>  implements Serializable {

    private static final long serialVersionUID = 2121211L;
    private UUID exchangeId;
	private String timestamp;
	private T payload;
	
}
