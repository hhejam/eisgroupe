package com.eisgroupe.word.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.common.domain.exchange.WordExchange;
import com.eisgroupe.word.domain.Word;

@Mapper(componentModel = "spring", uses = {ClientMapper.class})
public interface WordMapper {

	@Mapping(source = "word.client.id", target = "clientID")
	WordExchange to(Word word);
    
	@Mapping(source = "wordExchange.clientID", target = "client.id")
    Word from(WordExchange wordExchange);
}
