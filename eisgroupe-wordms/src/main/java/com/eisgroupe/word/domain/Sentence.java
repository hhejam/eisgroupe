package com.eisgroupe.word.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.SASI;
import org.springframework.data.cassandra.core.mapping.SASI.IndexMode;
import org.springframework.data.cassandra.core.mapping.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString 
@EqualsAndHashCode
@AllArgsConstructor
@Builder
@NoArgsConstructor
@XStreamAlias("sentence")
@Table
public class Sentence implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column 
    @PrimaryKey
    private String id;
    
    @Indexed
    @SASI(indexMode = IndexMode.CONTAINS)
    @SASI.StandardAnalyzed  
    private String value;
    
    @Column 
    private Instant creationTime;
    
    @XStreamAlias("client")
    @Transient
    private Client client;

    @Builder.Default
    @XStreamImplicit
    @Transient
    private List<Word> words = new ArrayList<Word>();
    
   
}
