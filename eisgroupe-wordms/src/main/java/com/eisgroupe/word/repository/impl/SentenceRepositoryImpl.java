package com.eisgroupe.word.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraOperations;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.cassandra.query.SentenceByClient;
import com.eisgroupe.word.domain.cassandra.query.SentenceByWord;
import com.eisgroupe.word.domain.mapper.cassandra.query.SentenceByClientMapper;
import com.eisgroupe.word.domain.mapper.cassandra.query.SentenceByWordMapper;
import com.eisgroupe.word.repository.cassandra.RepositoryBluckOperation;

public class SentenceRepositoryImpl implements RepositoryBluckOperation<Sentence> {

	@Autowired
	private CassandraOperations operations;
	
	@Autowired
	private SentenceByClientMapper sentenceByClientMapper;

	@Autowired
	private SentenceByWordMapper  sentenceByWordMapper;
	
	
	
	@Override
	public <S extends Sentence> S save(S sentence) {
		final CassandraBatchOperations batchOps = this.operations.batchOps();
		SentenceByClient sentenceByClient = this.sentenceByClientMapper.to(sentence);
		
		sentence.getWords()
				 .stream()
				 .forEach(word -> {
					 SentenceByWord sentenceByWord = this.sentenceByWordMapper.to(sentence, word);
					 batchOps.insert(sentenceByWord);
				 });
		
		
		batchOps.insert(sentenceByClient);
		batchOps.insert(sentence);
		
		batchOps.execute();
		return sentence;
	}

	
}
