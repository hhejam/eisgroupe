package com.eisgroupe.word.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraOperations;

import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.WordByClient;
import com.eisgroupe.word.domain.mapper.cassandra.query.WordByClientMapper;
import com.eisgroupe.word.repository.cassandra.RepositoryBluckOperation;

public class WordRepositoryImpl implements RepositoryBluckOperation<Word> {

	@Autowired
	private CassandraOperations operations;
	
	@Autowired
	private WordByClientMapper wordByClientMapper;

	@Override
	public <S extends Word> S save(S word) {
		final CassandraBatchOperations batchOps = this.operations.batchOps();
		WordByClient wordByClient = this.wordByClientMapper.to(word);
		batchOps.insert(word.getClient());
		batchOps.insert(wordByClient);
		batchOps.insert(word);
		batchOps.execute();
		return word;
	}

	
}
