package com.eisgroupe.processor.camel.processor;

/**
 * Interface to transform an object.
 * @author hakim
 *
 * @param <I>
 * @param <O>
 */
public interface Transfomer<I, O> {

	O transform(I input);
}
