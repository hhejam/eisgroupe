package com.eisgroupe.word.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.service.SearchFacadeService;

/**
 * REST controller for searching {@link Sentence} and {@link Word}.
 */
@RestController
@RequestMapping("/api/search")
public class SearchResource {

    private final Logger log = LoggerFactory.getLogger(SearchResource.class);

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private SearchFacadeService searchFacadeService;

    
    /**
     * {@code GET  /Sentences} : get all the Sentences by client id.
     *
     * @param client id
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Sentences in body.
     */
    @GetMapping("/sentences/client/{clientID}")
    public List<Sentence> searchSentenceByClient(@PathVariable String clientID) {
        log.debug("REST request to get all Sentences for the client {}", clientID);
        return searchFacadeService.searchSentenceByClient(clientID);
    }
    
    /**
     * {@code GET  /Sentences} : get all the Sentences by client id.
     *
     * @param word query
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Sentences in body.
     */
    @GetMapping("/sentences/{word}")
    public List<Sentence> searchSentenceByWordLike(@PathVariable String word) {
        log.debug("REST request to get all Sentences with word that contains {}", word);
        return searchFacadeService.searchSentenceByWordLike(word);
    }
    
    /**
     * {@code GET  /Words} : get all the words by client id.
     *
     * @param word query
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Sentences in body.
     */
    @GetMapping("/words/client/{clientID}")
    public List<Word> searchWordByClient(@PathVariable String clientID) {
        log.debug("REST request to get all word for the client {}", clientID);
        return searchFacadeService.searchWordByClient(clientID);
    }
    
    
    /**
     * {@code GET  /Words} : get all the words by client id.
     *
     * @param word query
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Sentences in body.
     */
    @GetMapping("/words/{word}")
    public List<Word> searchWordByValueLike(@PathVariable String word) {
        log.debug("REST request to get all word that contains {}", word);
        return searchFacadeService.searchWordByValueLike(word);
    }
    
    
}
