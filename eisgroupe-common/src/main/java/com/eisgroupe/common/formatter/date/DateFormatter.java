package com.eisgroupe.common.formatter.date;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateFormatter {

	private static final DateTimeFormatter formatter = 	
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
								 .withZone(ZoneId.systemDefault());

	public static Instant parse(String dateAsString) {
		return Instant.from(formatter.parse(dateAsString));
	}
	
	public static String parse(Instant dateAsInstant) {
		return  formatter.format( dateAsInstant );
	}
}
