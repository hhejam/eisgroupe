package com.eisgroupe.processor.camel.processor;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;

@Component(value = "exchangeMessageProcessor")
public class ExchangeMessageProcessor implements Processor {

	@Autowired
	private Transfomer<List<WordExchange>, MessageExchange<List<SentenceExchange>>> transformer;

	@Override
	@SuppressWarnings("unchecked")
	public void process(Exchange exchange) throws Exception {

		List<WordExchange> wordExchanges = (List<WordExchange>) exchange.getIn().getBody();
		MessageExchange<List<SentenceExchange>> messageExchange = this.transformer.transform(wordExchanges);
		exchange.getIn().setBody(messageExchange);
	}

}
