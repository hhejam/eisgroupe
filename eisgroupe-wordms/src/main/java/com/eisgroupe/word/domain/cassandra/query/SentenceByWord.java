package com.eisgroupe.word.domain.cassandra.query;

import java.io.Serializable;
import java.time.Instant;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.SASI;
import org.springframework.data.cassandra.core.mapping.SASI.IndexMode;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 * A domain class to To represent the query Cassandra for word by client.
 */
@Table
public class SentenceByWord implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    private SentenceByWordKey key;
    
    @Column
    @Indexed
    @SASI(indexMode = IndexMode.CONTAINS)
    @SASI.StandardAnalyzed
    private String sentence;
    
    @Column
    @Indexed
    @SASI(indexMode = IndexMode.CONTAINS)
    @SASI.StandardAnalyzed
    private String word;

    @Column
    private String clientID;
    
    @Column
    private Instant creationTime;

	public SentenceByWordKey getKey() {
		return key;
	}

	public void setKey(SentenceByWordKey key) {
		this.key = key;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}
    
}
