package com.eisgroupe.word;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.eisgroupe.word.domain.Client;
import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.repository.ClientRepository;
import com.eisgroupe.word.repository.SentenceByClientRepository;
import com.eisgroupe.word.repository.SentenceByWordRepository;
import com.eisgroupe.word.repository.SentenceRepository;
import com.eisgroupe.word.repository.WordByClientRepository;
import com.eisgroupe.word.repository.WordRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SmokTests {
	
	 private final Logger log = LoggerFactory.getLogger(SmokTests.class);

	private static final long POLL_INTERVAL_SECOND = 5;
	private static final long MAX_INTERVAL_MINIUTE = 3;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WordRepository wordRepository;

	@Autowired
	private SentenceRepository sentenceRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private SentenceByClientRepository sentenceByClientRepository;

	@Autowired
	private SentenceByWordRepository sentenceByWordRepository;

	@Autowired
	private WordByClientRepository wordByClientRepository;

	@BeforeEach
	public void cleanDatabase() {
		this.wordRepository.deleteAll();
		this.sentenceRepository.deleteAll();
		this.clientRepository.deleteAll();
		this.sentenceByClientRepository.deleteAll();
		this.sentenceByWordRepository.deleteAll();
		this.wordByClientRepository.deleteAll();
	}

	@Test
	void main() throws Exception {
		log.debug("Start the main method for the smoketests.");

		Client client1 = Client.builder().id("001").label("client-001").build();
		Client client2 = Client.builder().id("002").label("client-002").build();

		Word hello = Word.builder().value("Hello").client(client1).build();
		Word world = Word.builder().value("world!").client(client1).build();

		Word bonjour = Word.builder().value("Bonjour").client(client2).build();
		Word tout = Word.builder().value("tout").client(client2).build();
		Word le = Word.builder().value("le").client(client2).build();
		Word monde = Word.builder().value("monde").client(client2).build();

		step1SendFirstWordGroupe(hello, world);

		step2SendSecondWordGroupe(bonjour, tout, le, monde);
		
		step3SearchSentenceByWordLike("/api/search/sentences/" + "Hel", "Hello world!", 1);
		
		step4SearchSentenceByClient("/api/search/sentences/client/" + "002", "Bonjour tout le monde", 1);
		
		step5SearchWordByValueLike("/api/search/words/ello", "Hello", 1);
		
		step6SearchWordByClient("/api/search/words/client/001", hello, world);

	}

	private void step1SendFirstWordGroupe(Word... wordArgs) throws JsonProcessingException, Exception {
		log.debug("Start the step 1: Send 2 words...");
		
		this.sendWord(wordArgs);

		// Check from the database
		List<Word> words = this.wordRepository.findAll();
		assertEquals("[SmokTest][Step1]", 2, words.size());
		
		waitSentenceProcessing(1); 
		
		List<Sentence> sentences = this.sentenceRepository.findAll();
		assertNotNull(sentences);
		assertEquals("[SmokTest][Step1]", 1, sentences.size());
		assertEquals("[SmokTest][Step1]", "Hello world!", sentences.get(0).getValue());

		// Check from the API
		List<Sentence> sentenceFirstResult = requestAllSentences();
		
		assertNotNull("[SmokTest][Step1]", sentenceFirstResult);
		assertEquals("[SmokTest][Step1]", 1, sentenceFirstResult.size());
		Sentence sentence1 = sentenceFirstResult.get(0);
		assertEquals("[SmokTest][Step1]", "Hello world!", sentence1.getValue());
		log.debug("End of the step 1: [OK]");

	}
	
	private void step2SendSecondWordGroupe(Word... wordArgs) throws JsonProcessingException, Exception {
		log.debug("Start the step 2: Send 3 new words...");
		// Send a second list of word
		this.sendWord(wordArgs);

		List<Word> words = this.wordRepository.findAll();
		assertEquals("[SmokTest][Step2]", 6, words.size());

		waitSentenceProcessing(2);
		
		// Check from the database
		List<Sentence> sentences = this.sentenceRepository.findAll();
		
		assertNotNull("[SmokTest][Step2]", sentences);
		assertEquals("[SmokTest][Step2]", 2, sentences.size());
		List<String> sentenceValues = sentences.stream().map(s -> s.getValue()).collect(Collectors.toList());

		assertTrue("[SmokTest][Step2]", sentenceValues.contains("Hello world!"));
		assertTrue("[SmokTest][Step2]", sentenceValues.contains("Bonjour tout le monde"));

		// Check from tha API
		List<Sentence> sentenceFirstResult = requestAllSentences();
		assertNotNull("[SmokTest][Step2]", sentenceFirstResult);
		assertEquals("[SmokTest][Step2]", 2, sentenceFirstResult.size());
		sentenceValues = sentenceFirstResult.stream().map(s -> s.getValue()).collect(Collectors.toList());
		assertTrue("[SmokTest][Step2]", sentenceValues.contains("Hello world!"));
		assertTrue("[SmokTest][Step2]", sentenceValues.contains("Bonjour tout le monde"));
		
		log.debug("End of the step 2: [OK]");

	}
	
	private void step3SearchSentenceByWordLike(String uri, String sentenceAsValue, int expected) throws JsonProcessingException, Exception {
		String result = mockMvc.perform(get(uri).contentType("application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
		List<Sentence> sentences = objectMapper.readValue(result, new TypeReference<List<Sentence>>() {
		});
		assertNotNull("[SmokTest][Step3]", sentences);
		assertEquals("[SmokTest][Step3]", expected, sentences.size());
		assertEquals("[SmokTest][Step3]", sentenceAsValue, sentences.get(0).getValue());
		
	}
	
	private void step4SearchSentenceByClient(String uri, String sentenceAsValue, int expected) throws JsonProcessingException, Exception {
		String result = mockMvc.perform(get(uri).contentType("application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
		List<Sentence> sentences = objectMapper.readValue(result, new TypeReference<List<Sentence>>() {
		});
		assertNotNull("[SmokTest][Step4]", sentences);
		assertEquals("[SmokTest][Step4]", expected, sentences.size());
		assertEquals("[SmokTest][Step4]", sentenceAsValue, sentences.get(0).getValue());
		
	}
	
	private void step5SearchWordByValueLike(String uri, String expectedValue, int expectedSize) throws JsonProcessingException, Exception {
		String result = mockMvc.perform(get(uri).contentType("application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
		List<Word> words = objectMapper.readValue(result, new TypeReference<List<Word>>() {
		});
		assertNotNull("[SmokTest][Step5]", words);
		assertEquals("[SmokTest][Step5]", expectedSize, words.size());
		assertEquals("[SmokTest][Step5]", expectedValue, words.get(0).getValue());
	}
	
	private void step6SearchWordByClient(String uri, Word... wordExpected) throws JsonProcessingException, Exception {
		String result = mockMvc.perform(get(uri).contentType("application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
		List<Word> words = objectMapper.readValue(result, new TypeReference<List<Word>>() {
		});
		assertNotNull("[SmokTest][Step6]", words);
		assertEquals("[SmokTest][Step6]", wordExpected.length, words.size());
		words.stream()
			 .forEach(word -> 
			 	assertTrue("[SmokTest][Step6]", words.stream()
			 										 .map(w -> w.getValue())
			 										 .collect(Collectors.toList())
			 										 .contains(word.getValue())));
	}

	private void waitSentenceProcessing(int size) {
		log.debug("Waiting the processor to assemble word, the expexcted is " + size + " sentences");
		Awaitility.with()
		  .pollInterval(POLL_INTERVAL_SECOND, TimeUnit.SECONDS)
		  .atMost(MAX_INTERVAL_MINIUTE, TimeUnit.MINUTES)
		  .await().until( sentencesRepositorySize(), equalTo(size) );
		log.debug("End of the waiting time for the assembly");
	}

	private List<Sentence> requestAllSentences() throws UnsupportedEncodingException, Exception {
		String result = mockMvc.perform(get("/api/sentences").contentType("application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();
		List<Sentence> sentencesAsObject = objectMapper.readValue(result, new TypeReference<List<Sentence>>() {
		});
		return sentencesAsObject;

	}
	

	private void sendWord(Word... words) throws JsonProcessingException, Exception {
		for (Word word : words) {

			mockMvc.perform(
					post("/api/words").contentType("application/json").content(objectMapper.writeValueAsString(word)))
					.andExpect(status().is2xxSuccessful());

		}
		
	}
	
	private Callable<Integer> sentencesRepositorySize() {
	      return () ->sentenceRepository.findAll().size(); 
	}
}