package com.eisgroupe.word.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.service.SentenceService;

/**
 * REST controller for managing {@link Sentence}.
 */
@RestController
@RequestMapping("/api")
public class SentenceResource {

    private final Logger log = LoggerFactory.getLogger(SentenceResource.class);

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private SentenceService sentenceService;

    
    /**
     * {@code GET  /Sentences} : get all the Sentences.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Sentences in body.
     */
    @GetMapping("/sentences")
    public List<Sentence> getAllSentences() {
        log.debug("REST request to get all Sentences");
        return sentenceService.findAll();
    }

}
