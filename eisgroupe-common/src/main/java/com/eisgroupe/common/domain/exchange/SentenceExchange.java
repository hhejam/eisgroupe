package com.eisgroupe.common.domain.exchange;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter 
@ToString 
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XStreamAlias("sentence")
public class SentenceExchange implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String id;
    
    private String exchangeId;
    
    private String value;
    
    private String creationTime;
    
    private String clientID;

    @XStreamImplicit
    @Builder.Default
    private List<WordExchange> words = new ArrayList<WordExchange>();
    
   
}
