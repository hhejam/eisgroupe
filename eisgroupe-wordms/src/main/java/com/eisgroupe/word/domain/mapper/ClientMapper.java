package com.eisgroupe.word.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.eisgroupe.common.domain.exchange.ClientExchange;
import com.eisgroupe.word.domain.Client;

@Mapper(componentModel = "spring", uses = {WordMapper.class})
public interface ClientMapper {

	@Mapping(source = "label", target = "clientLabel")
    ClientExchange to(Client  client); 
    
	@Mapping(source = "clientLabel", target = "label")
    Client from(ClientExchange clientExchange);

}
