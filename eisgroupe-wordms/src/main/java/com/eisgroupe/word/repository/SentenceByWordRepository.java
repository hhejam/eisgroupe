package com.eisgroupe.word.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.cassandra.query.SentenceByWord;
import com.eisgroupe.word.domain.cassandra.query.SentenceByWordKey;

/**
 * Spring Data Cassandra repository for query the sentence by client Id.
 */
@Repository
public interface SentenceByWordRepository extends CassandraRepository<SentenceByWord, SentenceByWordKey> {

	@AllowFiltering
	List<SentenceByWord> findByKeySentenceID(String sentenceID);
	
	@AllowFiltering
	List<SentenceByWord> findByKeyWordID(String wordID);
	
	@AllowFiltering
	List<SentenceByWord> findByWordLike(String word);
	

}
