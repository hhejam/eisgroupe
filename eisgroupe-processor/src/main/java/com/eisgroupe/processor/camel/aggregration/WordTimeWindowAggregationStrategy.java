package com.eisgroupe.processor.camel.aggregration;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AbstractListAggregationStrategy;
import org.springframework.stereotype.Component;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;

@Component(value = "wordTimeWindowAggregationStrategy")
public class WordTimeWindowAggregationStrategy extends AbstractListAggregationStrategy<WordExchange> {

	@Override
	public WordExchange getValue(Exchange exchange) {
		return (WordExchange) exchange.getIn().getBody(MessageExchange.class).getPayload();
	}
}


