package com.eisgroupe.processor.camel.processor;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;

@Component
public class ExchangeMessageTransfomerImpl
		implements Transfomer<List<WordExchange>, MessageExchange<List<SentenceExchange>>> {

	@Override
	public MessageExchange<List<SentenceExchange>> transform(List<WordExchange> input) {
		Map<String, List<WordExchange>> wordByClient = 	this.groupWordByClient(input);
		Map<String, SentenceExchange> sentencesByClient = this.groupSentenceByClient(wordByClient);
		MessageExchange<List<SentenceExchange>> messageExchange =  this.buildMessageExchange(sentencesByClient);
		return messageExchange;
	}

	/**
	 * Build the exchange message with the list on sentence grouped by client.
	 * 
	 * @param sentencesByClient
	 * @return {@link MessageExchange}
	 */
	private MessageExchange<List<SentenceExchange>> buildMessageExchange(
			Map<String, SentenceExchange> sentencesByClient) {
		return MessageExchange.<List<SentenceExchange>>builder().timestamp(Instant.now().toString())
				.payload(new ArrayList<SentenceExchange>(sentencesByClient.values())).build();
	}

	/**
	 * Groupe the incomming Word by client
	 * 
	 * @param words
	 * @return {@link Map} key: the client id value: the {@link List} of
	 *         {@link WordExchange}
	 */
	private Map<String, List<WordExchange>> groupWordByClient(List<WordExchange> words) {
		return words.stream().collect(groupingBy(WordExchange::getClientID));
	}

	/**
	 * Groupe the sentences by Client
	 * 
	 * @param wordByClient
	 * @return {@link Map} Key: the client id value: {@link SentenceExchange}
	 */
	private Map<String, SentenceExchange> groupSentenceByClient(Map<String, List<WordExchange>> wordByClient) {
		return wordByClient.entrySet().stream().collect(toMap(Map.Entry::getKey, entry -> {
			return buildSentenceExchange(entry.getKey(), entry.getValue());
		}));
	}

	/**
	 * Build the {@link SentenceExchange}
	 * 
	 * @param clientID
	 * @param list     of {@link WordExchange}
	 * @return {@link SentenceExchange}
	 */
	private SentenceExchange buildSentenceExchange(String clientID, List<WordExchange> wordExchanges) {
		return SentenceExchange.builder().id(UUID.randomUUID().toString()).words(wordExchanges).clientID(clientID)
				.exchangeId(UUID.randomUUID().toString()).creationTime(Instant.now().toString())
				.value(wordConcatenation(clientID, wordExchanges)).build();
	}

	/**
	 * Simple join the word value for making the Sentence value
	 * 
	 * @param clientID
	 * @param words
	 * @return {@link String} representation of the sentence as value.
	 */
	private String wordConcatenation(String clientID, List<WordExchange> words) {
		StringBuilder builder = new StringBuilder();
		builder.append(words.stream().map(e -> e.getValue()).collect(Collectors.joining(" ")));

		return builder.toString();
	}

}
