package com.eisgroupe.common.domain.exchange;

public enum WordStatus {
	NEW, SENT_TO_PROCESSOR, 
	RECIVED_BY_PROCESSOR, JOINED_BY_PROCESSOR, READY, DELETED
}
