package com.eisgroupe.common.kafka.serialization.serde;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@Deprecated
public class POJODeserializer<T> implements Deserializer<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(POJODeserializer.class);
	
	private Class<T> type;
	
	public POJODeserializer() {
	}
	
	public POJODeserializer(Class<T> type) {
		this.type = type;
	}

	@Override
	public void close() {
	}

	@Override
	public void configure(Map<String, ?> arg0, boolean arg1) {
	}

	@Override
	public T deserialize(String topic, byte[] data) {
		LOGGER.info("Deserializing from topic {}", topic);
		ObjectMapper mapper = new ObjectMapper();
		T payload = null;
		try {
			payload = mapper.readValue(data,  this.type);
			LOGGER.info("Deserialized payload {}", payload);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return payload;
	}
}
