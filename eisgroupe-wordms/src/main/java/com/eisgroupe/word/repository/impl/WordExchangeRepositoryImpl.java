package com.eisgroupe.word.repository.impl;

import java.time.Instant;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.kafka.KafkaConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
import com.eisgroupe.common.formatter.date.DateFormatter;
import com.eisgroupe.word.repository.WordExchangeRepository;

@Repository(value = "WordRepositoty" )
public class WordExchangeRepositoryImpl implements WordExchangeRepository<WordExchange> {

	private final Logger log = LoggerFactory.getLogger(WordExchangeRepositoryImpl.class);

	@Autowired
	ProducerTemplate producerTemplate;

	@Override
	public <S extends WordExchange> S save(S entity) {
		log.info("Publishing a message on the word topic");
		MessageExchange<WordExchange> messageExchange =  MessageExchange.<WordExchange>builder()
														  .timestamp(DateFormatter.parse(Instant.now()))
														  .payload(entity)
														  .build();
		producerTemplate.sendBodyAndHeader("direct:start-word",
											messageExchange, 
											KafkaConstants.TIMESTAMP, 
											System.currentTimeMillis());
		return entity;
	}

	


}
