package com.eisgroupe.processor.kafka.aggregration;

import org.apache.kafka.streams.kstream.Initializer;

import com.eisgroupe.common.domain.exchange.SentenceExchange;

@Deprecated
public class WordAggregatorInitializer implements Initializer<SentenceExchange> {

	@Override
	public SentenceExchange apply() {
		return new SentenceExchange();
	}

}