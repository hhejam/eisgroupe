package com.eisgroupe.common.kafka.serialization.serde;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@Deprecated
public class POJOSerializer<T> implements Serializer<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(POJOSerializer.class);
	
	@Override
	public void configure(Map<String, ?> map, boolean b) {
	}

	@Override
	public byte[] serialize(String arg0, T arg1) {
		LOGGER.info("Serializing message wrapper {}", arg1);
		byte[] retVal = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			retVal = objectMapper.writeValueAsString(arg1).getBytes();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return retVal;
	}

	@Override
	public void close() {
	}
}