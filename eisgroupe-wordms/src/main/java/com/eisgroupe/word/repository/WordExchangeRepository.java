package com.eisgroupe.word.repository;

public interface WordExchangeRepository<T> {
	
	/**
	 * Save a given entity by sending to a topic. 
	 * @param entity must not be {@literal null}.
	 * @return the saved entity; will never be {@literal null}.
	 * @throws IllegalArgumentException in case the given {@literal entity} is {@literal null}.
	 */
	<S extends T> S save(S entity);


}
