package com.eisgroupe.word.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import com.eisgroupe.common.domain.exchange.ClientExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
import com.eisgroupe.word.domain.Client;
import com.eisgroupe.word.domain.Sentence;
import com.eisgroupe.word.domain.Word;
import com.eisgroupe.word.domain.cassandra.query.WordByClient;
import com.eisgroupe.word.domain.mapper.ClientMapper;
import com.eisgroupe.word.domain.mapper.SentenceMapper;
import com.eisgroupe.word.domain.mapper.WordMapper;
import com.eisgroupe.word.domain.mapper.cassandra.query.WordByClientMapper;

@SpringBootTest
@EnableAutoConfiguration(exclude = { KafkaAutoConfiguration.class, CassandraRepositoriesAutoConfiguration.class })
public class ModelMapperTest {
	@Autowired
	private ClientMapper clientMapper;

	@Autowired
	private WordMapper wordMapper;

	@Autowired
	private SentenceMapper sentenceMapper;
	
	@Autowired
	private WordByClientMapper wordByClientMapper;


	private Instant currentTimestamp = Instant.now();

	private Word word1 = Word.builder().creationTime(currentTimestamp).id(UUID.randomUUID().toString())
			.value("word1").build();

	private Word word2 = Word.builder().creationTime(currentTimestamp).id(UUID.randomUUID().toString())
			.value("word2").build();

	private Word word3 = Word.builder().creationTime(currentTimestamp).id(UUID.randomUUID().toString())
			.value("word3").build();

	private Client client = Client.builder().id(UUID.randomUUID().toString()).label("client-001").build();

	private Sentence sentence = Sentence.builder().id(UUID.randomUUID().toString()).client(client)
			.creationTime(currentTimestamp).words(Arrays.asList(word1, word2, word3)).build();

	@Test
	public void clientToClientExchange() {

		ClientExchange clientExchange = this.clientMapper.to(client);

		// then
		assertNotNull(clientExchange);
		assertEquals(client.getId(), clientExchange.getId());
		assertEquals(client.getLabel(), clientExchange.getClientLabel());

		Client clientTarget = this.clientMapper.from(clientExchange);
		assertNotNull(clientTarget);
		assertEquals(clientTarget.getId(), clientExchange.getId());
		assertEquals(clientTarget.getLabel(), clientExchange.getClientLabel());

	}

	@Test
	public void wordToWordExchange() {
		// given
		this.word1.setClient(client);

		// when
		WordExchange wordExchange = this.wordMapper.to(word1);

		// then
		assertNotNull(wordExchange);
		assertEquals(word1.getId(), wordExchange.getId());
		assertEquals(word1.getValue(), wordExchange.getValue());
		assertEquals(word1.getClient().getId(), wordExchange.getClientID());

		Word wordTarget = this.wordMapper.from(wordExchange);

		assertNotNull(wordTarget);
		assertEquals(wordTarget.getId(), wordExchange.getId());
		assertEquals(wordTarget.getValue(), wordExchange.getValue());
		assertEquals(wordTarget.getClient().getId(), wordExchange.getClientID());

	}

	@Test
	public void sentenceToSentenceExchange() {

		// when
		SentenceExchange sentenceExchange = this.sentenceMapper.to(sentence);

		// then
		assertNotNull(sentence);
		assertEquals(sentence.getId(), sentenceExchange.getId());
		assertEquals(sentence.getValue(), sentenceExchange.getValue());
		assertEquals(sentence.getClient().getId(), sentenceExchange.getClientID());

		Sentence sentenceTarget = this.sentenceMapper.from(sentenceExchange);

		// then
		assertNotNull(sentenceTarget);
		assertEquals(sentenceTarget.getId(), sentenceExchange.getId());
		assertEquals(sentenceTarget.getValue(), sentenceExchange.getValue());
		assertEquals(sentenceTarget.getClient().getId(), sentenceExchange.getClientID());

	}
	
	
	@Test
	public void wordToWordByClient() {
		//
		this.word1.setClient(client);
		
		// when
		WordByClient wordByClient  = this.wordByClientMapper.to(word1);

		// then
		assertNotNull(wordByClient);
	}
	

}
