package com.eisgroupe.word.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.eisgroupe.word.domain.cassandra.query.SentenceByClient;
import com.eisgroupe.word.domain.cassandra.query.SentenceByClientKey;

/**
 * Spring Data Cassandra repository for query the sentence by client Id.
 */
@Repository
public interface SentenceByClientRepository extends CassandraRepository<SentenceByClient, SentenceByClientKey> {

	@AllowFiltering
	List<SentenceByClient> findByKeyClientID(String clientID);

}
