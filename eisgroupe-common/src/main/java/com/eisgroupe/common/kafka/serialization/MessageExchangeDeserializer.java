package com.eisgroupe.common.kafka.serialization;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eisgroupe.common.domain.exchange.ClientExchange;
import com.eisgroupe.common.domain.exchange.MessageExchange;
import com.eisgroupe.common.domain.exchange.SentenceExchange;
import com.eisgroupe.common.domain.exchange.WordExchange;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;

/**
 * Custom Kafka deserializer for MessageWrapper
 *
 */
public class MessageExchangeDeserializer<T> implements Deserializer<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageExchangeDeserializer.class);
	private XStream xstream = new XStream(new StaxDriver());
	
	@Override
	public void close() {
	}

	@Override
	public void configure(Map<String, ?> arg0, boolean arg1) {
		xstream.autodetectAnnotations(true);
		XStream.setupDefaultSecurity(xstream);
		xstream.addPermission(AnyTypePermission.ANY);
		xstream.ignoreUnknownElements();
		xstream.processAnnotations(MessageExchange.class);
		xstream.processAnnotations(ClientExchange.class);
		xstream.processAnnotations(SentenceExchange.class);
		xstream.processAnnotations(WordExchange.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T deserialize(String topic, byte[] data) {
		LOGGER.info("Deserializing from topic {}", topic);
		T payload = null;
		try {
			payload = (T) xstream.fromXML(new String(data)); 
			LOGGER.info("Deserialized payload {}", payload);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return payload;
	}
}
