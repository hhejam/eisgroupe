package com.eisgroupe.word.config;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.KeyspaceOption;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories(basePackages = "com.eisgroupe")
public class CassandraConfig extends AbstractCassandraConfiguration implements BeanClassLoaderAware {

	@Value("${spring.data.cassandra.contact-points}")
	private String contactPoints;

	@Value("${spring.data.cassandra.port}")
	private int port;

	@Value("${spring.data.cassandra.keyspace-name}")
	private String keySpace;

	@Value("${spring.data.cassandra.username}")
	private String username;

	@Value("${spring.data.cassandra.password}")
	private String password;

	@Value("${spring.data.cassandra.schema-action}")
	private String schemaAction;

	@Value("${spring.data.cassandra.local-datacenter}")
	private String dataCenter;

	@Override
	protected List<CreateKeyspaceSpecification> getKeyspaceCreations() {
		if (!StringUtils.equalsIgnoreCase(SchemaAction.NONE.name(), schemaAction)) {
			CreateKeyspaceSpecification specification = CreateKeyspaceSpecification.createKeyspace(getKeyspaceName())
					.with(KeyspaceOption.DURABLE_WRITES, true).withSimpleReplication();

			return Arrays.asList(specification);
		}
		return super.getKeyspaceCreations();
	}
	
	@Override
	public String[] getEntityBasePackages() {
		return new String[] { "com.eisgroupe" };
	}

	@Override
	protected String getContactPoints() {
		return contactPoints;
	}

	@Override
	protected int getPort() {
		return port;
	}

	@Override
	public SchemaAction getSchemaAction() {
		return SchemaAction.valueOf(schemaAction);
	}

	@Override
	protected String getLocalDataCenter() {
		return dataCenter;
	}

	@Override
	protected String getKeyspaceName() {
		return keySpace;
	}

}
